//NAVBAR
const logo = document.getElementById('homelogo');
const homeNavbar = document.getElementById('homeNavbar');
const prodNavbar = document.getElementById('prodNavbar');
const loginNavbar = document.getElementById('loginNavbar');


logo.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./homepage.html')

})

homeNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./homepage.html')

})
prodNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./catalog.html')

})

loginNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./login.html')

})

//PRODUCT CATALOG



let params = new URLSearchParams(document.location.search);

const productId = params.get("productId")

fetch(`https://dmb-steel.herokuapp.com/api/products/${productId}`, {
	method: "GET",
})

.then(result => result.json())
.then(result => {

    prodName = result.name
    description = result.description
    weight = result.weight
    length = result.length
    quantity = result.quantity
    price = result.price

const prodDetails = document.getElementById('productDeets');

    const prodCard = 
            `
                <div class="col-12 my-2 d-flex justify-content-center">
                    <div class="card bg-dark rounded" style="width: 35rem;">
                        <div class="card-body text-center">
                            <h2 class="card-title text-light">
                                ${description}
                            </h2>
                            <img src="images/ibeam.jpg" class="img.fluid ibeam">
                            <p class="card-text text-light my-3">
                            Weight: ${weight} lbs/ft
                            </p>
                            <p class="card-text text-light my-3">
                            Length: ${length} ft
                            </p>
                            <p class="card-text text-light my-3">
                            Price: &#8369; ${price}
                            </p>
                            <p class="card-text text-light my-3">
                            Stocks left: ${quantity}
                            </p>
                            <a 
                                class="btn btn-info"
                                href="./register.html">
                                Buy now
                            </a>

                        </div>
                    </div>
                </div>
            `
    console.log(prodCard)


        prodDetails.innerHTML = prodCard
})



    


