//NAVBAR
const logo = document.getElementById('homelogo');
const homeNavbar = document.getElementById('homeNavbar');
const prodNavbar = document.getElementById('prodNavbar');
const loginNavbar = document.getElementById('loginNavbar');


logo.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./homepage.html')

})

homeNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./homepage.html')

})
prodNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./catalog.html')

})

loginNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./login.html')

})

// REGISTRATION FORM


const signUpForm = document.getElementById('signup');

signUpForm.addEventListener("signup", (e) => {
	e.preventDefault()

    //FORM VALUES
    const fName = document.getElementById('fName').value;
	const lName = document.getElementById('lName').value;
	const email = document.getElementById('email').value;
	const password = document.getElementById('password').value;
	const cpw = document.getElementById('cpw').value;

    if(password === cpw){

		fetch(`https://dmb-steel.herokuapp.com/api/users/email-exists`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: fName,
				lastName: lName,
				email: email,
				password: password
			})
		})
		.then( result => result.json())
		.then(result => {

			if(result == false){
				fetch(`https://dmb-steel.herokuapp.com/api/users/register`, {
					method: "POST",
					headers:{
						"Content-Type": "application/json",
					},
					body: JSON.stringify({
						firstName: fName,
                        lastName: lName,
                        email: email,
                        password: password
					})
				})
				.then(result => result.json())
				.then(result => {
					if(result){
						alert('User successfully registered!')

						window.location.replace('./login.html')
					} else {
						alert(`Please try again`)
					}
				})

			} else {
				alert(`User already exists`)
			}
		})
	}


})
