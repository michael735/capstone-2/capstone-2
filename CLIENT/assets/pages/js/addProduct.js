//NAVBAR
const logo = document.getElementById('homelogo');
const homeNavbar = document.getElementById('homeNavbar');
const prodNavbar = document.getElementById('prodNavbar');
const loginNavbar = document.getElementById('loginNavbar');


logo.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./adminDashboard.html')

})

homeNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./adminDashboard.html')

})
prodNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./userCatalog.html')

})

loginNavbar.addEventListener("click", (e) => {	e.preventDefault()

    localStorage.removeItem(`token`)
    localStorage.removeItem(`id`)
    localStorage.removeItem(`admin`)

    window.location.replace('./login.html')

})


// ADD PRODUCT

const token = localStorage.getItem('token')
const isAdmin = localStorage.getItem(`admin`)

const productForm = document.getElementById('productForm')

productForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let name = document.getElementById('name').value
	let desc = document.getElementById('description').value
    let weight = document.getElementById('weight').value
    let rate = document.getElementById('rate').value
	let length = document.getElementById('length').value
    let qty = document.getElementById(`quantity`).value


	fetch(`https://dmb-steel.herokuapp.com/api/products/listProduct`, {
		method: "POST",
		headers:{
			"Content-Type": "application/json",
			"Authorization": `Bearer ${token}`
		},
		body: JSON.stringify({
			name: name,
			description: desc, 
            weight: weight,
            rate : rate,
            length : length,
            quantity : qty	
		})
	})
	.then(result => result.json())
	.then(result => {
		console.log(result)

		if(result){
			alert(`Product successfully added`)

                window.location.replace('./userCatalog.html')
	
		} else {
			alert('Something went wrong')
		}
	})

})
