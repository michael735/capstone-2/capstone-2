//NAVBAR
const logo = document.getElementById('homelogo');
const homeNavbar = document.getElementById('homeNavbar');
const prodNavbar = document.getElementById('prodNavbar');
const loginNavbar = document.getElementById('loginNavbar');
const isAdmin = localStorage.getItem(`admin`)

logo.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./adminDashboard.html')

})

homeNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./adminDashboard.html')

})
prodNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./userCatalog.html')

})

loginNavbar.addEventListener("click", (e) => {	e.preventDefault()

    localStorage.removeItem(`token`)
    localStorage.removeItem(`id`)
    localStorage.removeItem(`admin`)

    window.location.replace('./login.html')

})

//PRODUCT DETAILS

const token = localStorage.getItem(`token`)


let params = new URLSearchParams(document.location.search);

const productId = params.get("productId")

fetch(`https://dmb-steel.herokuapp.com/api/products/${productId}`, {
	method: "GET",
})

.then(result => result.json())
.then(result => {
    
    prodName = result.name;
    description = result.description;
    weight = result.weight;
    length = result.length;
    stocks = result.quantity;
    price = result.price;
    isActive = result.isActive;

//ARCHIVE PRODUCTS

const editProduct = document.getElementById(`editProduct`);

editProduct.addEventListener("click", (e) =>{ e.preventDefault()
    window.location.replace(`./editProduct.html?productId=${productId}`) 
})
const archive = document.getElementById(`archive`);
if(isActive == true){

    archive.innerHTML = `<a class="btn bg-warning" id="archiveProduct">
                         Archive
                         </a>`;

    const archiveProduct = document.getElementById(`archiveProduct`);


    archiveProduct.addEventListener("click", (e) => {	e.preventDefault()

        fetch(`https://dmb-steel.herokuapp.com/api/products/${productId}/archive`,{
            method: "PATCH",
            headers: {
				"Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            }
        })
        window.location.replace(`./adminProductdetails.html?productId=${productId}`)
    })

    
//UNARCHIVE PRODUCTS
} else{
    archive.innerHTML = `<a class="btn bg-success" id="unarchiveProduct">
                         Unarchive
                         </a>`
    unarchiveProduct.addEventListener("click", (e) => {	e.preventDefault()

        fetch(`https://dmb-steel.herokuapp.com/api/products/${productId}/unarchive`,{
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
                }
        })

        window.location.replace(`./adminProductdetails.html?productId=${productId}`)
    })
        

}

//PRODUCT DETAILS
const prodDetails = document.getElementById('productDeets');

    const prodCard = 
                     `
                        <h2 class="card-title text-light">
                            ${description}
                        </h2>
                        <img src="images/ibeam.jpg" class="img.fluid ibeam">
                        <p class="card-text text-light my-3">
                        Weight: ${weight} lbs/ft
                        </p>
                        <p class="card-text text-light my-3">
                        Length: ${length} ft
                        </p>
                        <p class="card-text text-light my-3">
                        Price: &#8369; ${price}
                        </p>
                        <p class="card-text text-light my-3">
                        Stocks left: ${stocks}
                        </p>
                      `

prodDetails.innerHTML = prodCard


    
})


const deleteProduct = document.getElementById(`deleteProduct`);

deleteProduct.addEventListener("click", (e) => {	e.preventDefault()

    fetch(`hhttps://dmb-steel.herokuapp.com/api/products/${productId}/delete`,{
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`
            }
    })

    window.location.replace(`./userCatalog.html`)
})
