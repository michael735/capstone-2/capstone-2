//NAVBAR
const logo = document.getElementById('homelogo');
const homeNavbar = document.getElementById('homeNavbar');
const prodNavbar = document.getElementById('prodNavbar');
const loginNavbar = document.getElementById('loginNavbar');


logo.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./homepage.html')

})

homeNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./homepage.html')

})
prodNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./catalog.html')

})

loginNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./login.html')

})

//PRODUCT CATALOG

const productContainer = document.getElementById('prodContainer')
let products;

fetch(`https://dmb-steel.herokuapp.com/api/products/isActive`,{
    method: "GET"
}).then(result => result.json())
.then(result => {

    if(result.length < 1){
        return `No Products Available`

    } else {

        products = result.map(product => {
            const {description, price,_id} = product

    

            return(
                `
                    <div class="col-12 col-lg-4 my-2 d-flex justify-content-center">
                        <div class="card bg-dark rounded" style="width: 18rem;">
                            <div class="card-body text-center">
                                <h5 class="card-title text-light">
                                    ${description}
                                </h5>
                                <img src="images/ibeam.jpg" class="img.fluid ibeam">
                                <p class="card-text text-light my-3">
                                 &#8369; ${price}
                                </p>
                                <a 
                                    class="btn btn-info"
                                    href="./productDetails.html?productId=${_id}">
                                    See more
                                </a>
                            </div>
                        </div>
                    </div>
                `
            )
        }).join(" ")


        productContainer.innerHTML = products

        
    }
    
})

