//NAVBAR
const logo = document.getElementById('homelogo');
const homeNavbar = document.getElementById('homeNavbar');
const prodNavbar = document.getElementById('prodNavbar');
const loginNavbar = document.getElementById('loginNavbar');


logo.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./userDashboard.html')

})

homeNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./userDashboard.html')

})
prodNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./userCatalog.html')

})

loginNavbar.addEventListener("click", (e) => {	e.preventDefault()

    localStorage.removeItem(`token`)
    localStorage.removeItem(`id`)
    localStorage.removeItem(`admin`)

    window.location.replace('./login.html')

})


//DASHBOARD BODY

const productContainer = document.getElementById('prodContainer')
const dashTitle = document.getElementById(`dashTitle`)
const myCart = document.getElementById(`myCart`)
const myOrders = document.getElementById(`myOrders`)
const token = localStorage.getItem(`token`)
const userId = localStorage.getItem(`id`)
const cartList = document.getElementById(`cartList`)
const orderList = document.getElementById(`orderList`)
let cart;
let orders;



dashTitle.innerHTML = `USER DASHBOARD`
myCart.innerHTML = `My Cart`
myOrders.innerHTML = `My Orders`

fetch(`https://dmb-steel.herokuapp.com//api/orders/myOrders`,{
    method: "GET",
    headers:{
        "Authorization": `Bearer ${token}`
    }
}).then(result => result.json())
.then(result => {

    console.log(result)

    if(result < 1){
        cartList.innerHTML = `<p class=text-warning>No existing items on cart</p>`
    }else{
        cart = result.map(cartOrder =>{
            const{description, price, quantity, bill, isActive,_id} = cartOrder

            if(isActive == false)
            return(
                `
                <div class="col-12 col-lg-4 mt-3 mb-5 d-flex justify-content-center">
                    <div class="card bg-light rounded" style="width: 18rem;">
                        <div class="card-body text-center">
                            <h5 class="card-title text-dark">
                                ${description}
                            </h5>
                            <img src="images/ibeam.jpg" class="img.fluid ibeam">
                            <p class="card-text text-dark my-3">
                            &#8369; ${price}
                            </p>
                            <p class="card-text text-dark">
                            Quantity: ${quantity}
                            </p>
                            <p class="card-text text-dark">
                            Bill Amount: &#8369; ${bill}
                            <a class="btn bg-info" href="editOrder.html?orderId=${_id}">
                            Edit
                            </a>
                            
                        </div>
                    </div>
                </div>
                 `
            )
        }).join(" ")

        cartList.innerHTML = cart
    
    }
})


const checkout = document.getElementById(`checkout`)

checkout.addEventListener(`click`, (e) => {e.preventDefault()

    fetch(`https://dmb-steel.herokuapp.com/api/orders/myOrders`,{
    method: "GET",
    headers:{
        "Authorization": `Bearer ${token}`
    }
    }).then(result => result.json())
    .then(result => {
        console.log(result)
        result.map(orderId =>{
            const {_id} = orderId
            fetch(`https://dmb-steel.herokuapp.com/api/orders/${_id}/unarchive`,{
                method: "PATCH",
                headers:{
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${token}`
                }
              
            })
        })
    })

    fetch(`https://dmb-steel.herokuapp.com/api/carts/create`,{
        method: "POST",
        headers:{
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`
        }
        
    })
    alert `Order successfully made`
    window.location.replace('./userDashboard.html')
})


fetch(`https://dmb-steel.herokuapp.com/api/carts/myCart`,{
    method: "GET",
    headers:{
        "Authorization": `Bearer ${token}`
    }
}).then(result => result.json())
.then(result => {

    console.log(result)

    orders = result.map(cartOrder =>{

        
        const{_id, totalBill, active} = cartOrder

        
        if(result < 1){
            orderList.innerHTML = `<p class=text-warning>No existing items on cart</p>`
        }else{
            
            if (active == true){

                return (
                    `
                    <div class="col-12 col-lg-4 mt-3 mb-5 d-flex justify-content-center">
                        <div class="card bg-light rounded" style="width: 18rem;">
                            <div class="card-body text-center">
                                <h5 class="card-title text-dark">
                                Order ID:  ${_id}
                                </h5>
                                <img src="images/ibeam.jpg" class="img.fluid ibeam">
                                <p class="card-text text-dark my-3">
                                    Bill Amount: &#8369; ${totalBill}
                                </p>
                                <p class="card-text text-dark">
                                <div class="mx-auto d-block text-center">
                                    <a class="btn bg-info" href="./cartDetails.html?cartId=${_id}">See details</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    `
                )
            }
        }
    }).join(" ")

    orderList.innerHTML = orders
})
    









