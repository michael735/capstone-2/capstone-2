//NAVBAR
const logo = document.getElementById('homelogo');
const homeNavbar = document.getElementById('homeNavbar');
const prodNavbar = document.getElementById('prodNavbar');
const loginNavbar = document.getElementById('loginNavbar');


logo.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./userDashboard.html')

})

homeNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./userDashboard.html')

})
prodNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./userCatalog.html')

})

loginNavbar.addEventListener("click", (e) => {	e.preventDefault()

    localStorage.removeItem(`token`)
    localStorage.removeItem(`id`)
    localStorage.removeItem(`admin`)

    window.location.replace('./login.html')

})


// EDIT PRODUCT



const token = localStorage.getItem('token')
let params = new URLSearchParams(document.location.search);
const orderId = params.get("orderId")

const desc = document.getElementById('description')
const price = document.getElementById('price')
const currentQty = document.getElementById('currentQty')
const bill = document.getElementById(`bill`)
const newQty = document.getElementById('newQty')
const sendNewQty = document.getElementById('sendNewQty')
const deleteOrder = document.getElementById(`deleteOrder`)


fetch(`https://dmb-steel.herokuapp.com/api/orders/${orderId}`, {
	method: "GET",
    headers:{
        "Authorization": `Bearer ${token}`
    }
}).then(result => result.json())
.then(result => {

    desc.innerHTML = result.description
    price.innerHTML = `&#8369; ${result.price}`
    bill.innerHTML = `&#8369; ${result.bill}`
    currentQty.innerHTML = `${result.quantity} pcs`


    sendNewQty.addEventListener("click", (e) => {
        e.preventDefault()

        
        console.log(newQty.value)
        console.log(result.price)
        fetch(`https://dmb-steel.herokuapp.com/api/orders/${orderId}/editQty`, {
            method: "PATCH",
            headers:{
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            },
            body: JSON.stringify({
                quantity : newQty.value,
                price : result.price
            })
        })
        .then(result => result.json())
        .then(result => {
    
            if(result){
                alert(`Order successfully updated`)
    
                    window.location.replace('./userDashboard.html')
        
            } else {
                alert('Something went wrong')
            }
        })
    
    })
      
})


deleteOrder.addEventListener("click", (e) => {
    e.preventDefault()

    fetch(`https://dmb-steel.herokuapp.com/api/orders/${orderId}/delete`, {
        method: "DELETE",
        headers:{
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`
        },

    })

    window.location.replace('./userDashboard.html')

})




