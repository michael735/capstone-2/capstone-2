//NAVBAR
const logo = document.getElementById('homelogo');
const homeNavbar = document.getElementById('homeNavbar');
const prodNavbar = document.getElementById('prodNavbar');
const loginNavbar = document.getElementById('loginNavbar');
const isAdmin = localStorage.getItem(`admin`)

if(isAdmin == "true"){
    logo.addEventListener("click", (e) => {	e.preventDefault()

        window.location.replace('./adminDashboard.html')
    
    })
    
    homeNavbar.addEventListener("click", (e) => {	e.preventDefault()
    
        window.location.replace('./adminDashboard.html')
    
    })
}else{
logo.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./userDashboard.html')

})

homeNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./userDashboard.html')

})
}
prodNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./userCatalog.html')

})

loginNavbar.addEventListener("click", (e) => {	e.preventDefault()

    localStorage.removeItem(`token`)
    localStorage.removeItem(`id`)
    localStorage.removeItem(`admin`)

    window.location.replace('./login.html')

})

//PRODUCT CATALOG

const productContainer = document.getElementById('prodContainer')
const adminMenu = document.getElementById(`addProduct`)
const token = localStorage.getItem(`token`)

let products;



if(isAdmin == "true"){

    adminMenu.innerHTML =
	`
		<a class="btn bg-success d-block text-center mx-auto mb-5" href="./addProduct.html">Add Product</a>

	`;

    
    fetch(`https://dmb-steel.herokuapp.com/api/products/`,{
        method: "GET"
        }).then(result => result.json())
        .then(result => {
    
                if(result.length < 1){
                    return `No Products Available`
    
            } else {
    
                products = result.map(product => {
                    const {description, price,_id,quantity,isActive} = product

                    if(quantity == 0 || isActive == false){
                        return (
                            `
                                <div class="col-12 col-lg-4 mt-3 mb-5 d-flex justify-content-center">
                                    <div class="card bg-secondary opacity-25 rounded" style="width: 18rem;">
                                        <div class="card-body text-center">
                                            <h5 class="card-title text-light">
                                                ${description}
                                            </h5>
                                            <img src="images/ibeam.jpg" class="img.fluid ibeam">
                                            <p class="card-text text-light my-3">
                                            &#8369; ${price}
                                            </p>
                                            <p class="card-text text-danger">
                                            Stocks left: ${quantity}
                                            </p>
                                            <a 
                                                class="btn btn-info"
                                                href="./adminProductDetails.html?productId=${_id}">
                                                Edit
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            `
                       )  
                    }else{
                        return(
                            `
                                <div class="col-12 col-lg-4 mt-3 mb-5 d-flex justify-content-center">
                                    <div class="card bg-dark rounded" style="width: 18rem;">
                                        <div class="card-body text-center">
                                            <h5 class="card-title text-light">
                                                ${description}
                                            </h5>
                                            <img src="images/ibeam.jpg" class="img.fluid ibeam">
                                            <p class="card-text text-light my-3">
                                            &#8369; ${price}
                                            </p>
                                            <p class="card-text text-light">
                                            Stocks left: ${quantity}
                                            </p>
                                            <a 
                                                class="btn btn-info"
                                                href="./adminProductDetails.html?productId=${_id}">
                                                Edit
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            `
                        )
                    }

                
            })     .join(" ")

            productContainer.innerHTML = products
             
        }
    })
    

}else{
    fetch(`https://dmb-steel.herokuapp.com/api/products/isActive`,{
        method: "GET"
        }).then(result => result.json())
        .then(result => {

            console.log(result)
    
            if(result.length < 1){
                return `No Products Available`
    
            } else {
    
                products = result.map(product => {
                    const {description, price,_id,quantity} = product



                    if(quantity == 0){
                        fetch(`https://dmb-steel.herokuapp.com/api/products/${_id}/archive`,{
                            method: "PATCH",
                            headers: {
                                "Content-Type": "application/json",
                                "Authorization": `Bearer ${token}`
                            }
                        }).then(result => result.json())
                        .then(result => result)
                
                    }else{
                        return(
                            `
                                <div class="col-12 col-lg-4 mt-3 mb-5 d-flex justify-content-center">
                                    <div class="card bg-dark rounded" style="width: 18rem;">
                                        <div class="card-body text-center">
                                            <h5 class="card-title text-light">
                                                ${description}
                                            </h5>
                                            <img src="images/ibeam.jpg" class="img.fluid ibeam">
                                            <p class="card-text text-light my-3">
                                            &#8369; ${price}
                                            </p>
                                            <p class="card-text text-light">
                                            Stocks left: ${quantity}
                                            </p>
                                            <a 
                                                class="btn btn-info"
                                                href="./userProductDetails.html?productId=${_id}">
                                                See more
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            `
                        )
                    }   
                
                }).join(" ")

            productContainer.innerHTML = products         
        }   
    })
    

}


