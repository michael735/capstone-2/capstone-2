//NAVBAR
const logo = document.getElementById('homelogo');
const homeNavbar = document.getElementById('homeNavbar');
const prodNavbar = document.getElementById('prodNavbar');
const loginNavbar = document.getElementById('loginNavbar');


logo.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./adminDashboard.html')

})

homeNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./adminDashboard.html')

})
prodNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./userCatalog.html')

})

loginNavbar.addEventListener("click", (e) => {	e.preventDefault()

    localStorage.removeItem(`token`)
    localStorage.removeItem(`id`)
    localStorage.removeItem(`admin`)

    window.location.replace('./login.html')

})


//ADMIN DASHBOARD

dashTitle.innerHTML = `ADMIN DASHBOARD`
const orderList = document.getElementById(`orderList`)
const token = localStorage.getItem(`token`)
let orders;

fetch(`https://dmb-steel.herokuapp.com/api/carts/allCarts`,{
    method: "GET",
    headers:{
        "Authorization": `Bearer ${token}`
    }
}).then(result => result.json())
.then(result => {


    orders = result.map(cartOrder =>{

        
        const{_id, totalBill, active} = cartOrder

        
        if(result < 1){
            orderList.innerHTML = `<p class=text-warning>No existing orders</p>`
        }else{
            
            if (active == true){

                return (
                    `
                    <div class="col-12 col-lg-4 mt-3 mb-5 d-flex justify-content-center">
                        <div class="card bg-light rounded" style="width: 18rem;">
                            <div class="card-body text-center">
                                <h5 class="card-title text-dark">
                                Order ID:  ${_id}
                                </h5>
                                <img src="images/ibeam.jpg" class="img.fluid ibeam">
                                <p class="card-text text-dark my-3">
                                    Bill Amount: &#8369; ${totalBill}
                                </p>
                                <p class="card-text text-dark">
                                <div class="mx-auto d-block text-center">
                                    <a class="btn bg-info" href="./adminOrders.html?cartId=${_id}">See details</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    `
                )
            }else{

                return (
                    `
                    <div class="col-12 col-lg-4 mt-3 mb-5 d-flex justify-content-center">
                        <div class="card bg-secondary rounded" style="width: 18rem;">
                            <div class="card-body text-center text-light">
                                <h5 class="card-title text-dark">
                                Order ID:  ${_id}
                                </h5>
                                <img src="images/ibeam.jpg" class="img.fluid ibeam">
                                <p class="card-text text-light my-3">
                                    Bill Amount: &#8369; ${totalBill}
                                </p>
                                <p class="card-text text-dark">
                                <div class="mx-auto d-block text-center">
                                    <a class="btn bg-info" href="./adminOrders.html?cartId=${_id}">See details</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    `
                )
            }
        }
    }).join(" ")

    orderList.innerHTML = orders
})