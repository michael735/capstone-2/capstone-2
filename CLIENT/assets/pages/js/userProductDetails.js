//NAVBAR
const logo = document.getElementById('homelogo');
const homeNavbar = document.getElementById('homeNavbar');
const prodNavbar = document.getElementById('prodNavbar');
const loginNavbar = document.getElementById('loginNavbar');


logo.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./userDashboard.html')

})

homeNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./userDashboard.html')

})
prodNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./userCatalog.html')

})

loginNavbar.addEventListener("click", (e) => {	e.preventDefault()

    localStorage.removeItem(`token`)
    localStorage.removeItem(`id`)
    localStorage.removeItem(`admin`)

    window.location.replace('./login.html')

})

//PRODUCT DETAILS

const token = localStorage.getItem(`token`)

let params = new URLSearchParams(document.location.search);
const productId = params.get("productId")


fetch(`https://dmb-steel.herokuapp.com//api/products/${productId}`, {
	method: "GET",
})

.then(result => result.json())
.then(result => {
    
    prodName = result.name
    description = result.description
    weight = result.weight
    length = result.length
    stocks = result.quantity
    price = result.price

const prodDetails = document.getElementById('productDeets');

    const prodCard = 
                     `
                        <h2 class="card-title text-light">
                            ${description}
                        </h2>
                        <img src="images/ibeam.jpg" class="img.fluid ibeam">
                        <p class="card-text text-light my-3">
                        Weight: ${weight} lbs/ft
                        </p>
                        <p class="card-text text-light my-3">
                        Length: ${length} ft
                        </p>
                        <p class="card-text text-light my-3">
                        Price: &#8369; ${price}
                        </p>
                        <p class="card-text text-light my-3">
                        Stocks left: ${stocks}
                        </p>
                      `

prodDetails.innerHTML = prodCard

    const buyProduct = document.getElementById('buy');

    buyProduct.addEventListener("click", (e) => {	e.preventDefault()
    const buyQuantity = document.getElementById('buyQuantity').value;

        if(buyQuantity>stocks)
        {
            return alert(`Not enough stocks for your order`)
        }else{
            fetch(`https://dmb-steel.herokuapp.com//api/orders/create`,{
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${token}`
                },
                body: JSON.stringify({
                        productId: productId,
                        quantity: buyQuantity,
                })

            }).then(result => result.json())
            .then(result => {result,

                window.location.replace('./userDashboard.html')})

        }
    })

})


