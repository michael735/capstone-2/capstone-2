//NAVBAR
const logo = document.getElementById('homelogo');
const homeNavbar = document.getElementById('homeNavbar');
const prodNavbar = document.getElementById('prodNavbar');
const loginNavbar = document.getElementById('loginNavbar');


logo.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./userDashboard.html')

})

homeNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./userDashboard.html')

})
prodNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./userCatalog.html')

})

loginNavbar.addEventListener("click", (e) => {	e.preventDefault()

    localStorage.removeItem(`token`)
    localStorage.removeItem(`id`)
    localStorage.removeItem(`admin`)

    window.location.replace('./login.html')

})


//MY ORDERS

const token = localStorage.getItem(`token`)

let params = new URLSearchParams(document.location.search);
const orderContainer = document.getElementById(`orderContainer`)
const cartId = params.get("cartId")
let order;

fetch(`https://dmb-steel.herokuapp.com/api/carts/${cartId}`,{
    method: "GET",
    headers:{
        "Authorization": `Bearer ${token}`
    }
}).then(result => result.json())
.then(result => {

        order = result.orders.map(cartOrder =>{
            const{description, price, quantity, bill, isActive,_id} = cartOrder

            if(isActive == false)
            return(
                `
                <div class="col-12 col-lg-4 mt-3 mb-5 d-flex justify-content-center">
                    <div class="card bg-light rounded" style="width: 18rem;">
                        <div class="card-body text-center">
                            <h5 class="card-title text-dark">
                                ${description}
                            </h5>
                            <img src="images/ibeam.jpg" class="img.fluid ibeam">
                            <p class="card-text text-dark my-3">
                            &#8369; ${price}
                            </p>
                            <p class="card-text text-dark">
                            Quantity: ${quantity}
                            </p>
                            <p class="card-text text-dark">
                            Bill Amount: &#8369; ${bill}   
                        </div>
                    </div>
                </div>
                 `
            )
        }).join(" ")

        orderContainer.innerHTML = order
    

})
