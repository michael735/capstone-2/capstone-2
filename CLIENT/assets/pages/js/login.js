//NAVBAR
const logo = document.getElementById('homelogo');
const homeNavbar = document.getElementById('homeNavbar');
const prodNavbar = document.getElementById('prodNavbar');
const loginNavbar = document.getElementById('loginNavbar');


logo.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./homepage.html')

})

homeNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./homepage.html')

})
prodNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./catalog.html')

})

loginNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./login.html')

})


//LOGIN FORM

const loginForm = document.getElementById('login')

loginForm.addEventListener('submit', (e) => {
	e.preventDefault()

    let email = document.getElementById('email').value
    let password = document.getElementById('password').value

    fetch(`https://dmb-steel.herokuapp.com/api/users/login`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            email: email,
            password: password
		})
	})

    .then(result => result.json())
    .then(result => {

        console.log(result)
        auth = result.auth
        
        if(auth === `Authentication Failed`){
            alert(`User does not exist or Password is incorrect. Please try again`)

        }else if(result){
            localStorage.setItem('token', result.token)

            let token = localStorage.getItem('token')

            fetch(`https://dmb-steel.herokuapp.com/api/users/profile`, {
                method: "GET",
                headers:{
                    "Authorization": `Bearer ${token}`
                }
            })
            .then(result => result.json())
            .then(result => {
                console.log(result)

                let userId = result._id

                localStorage.setItem('id', result._id)
                localStorage.setItem('admin', result.isAdmin)

                if(result.isAdmin === true){
                    window.location.replace(`./adminDashboard.html`)
                }else{
                    window.location.replace(`./userDashboard.html?userId=${userId}`)
                }
            })

        }else{
            alert(`Something went wrong. Please try again`)
        }


    })               
})