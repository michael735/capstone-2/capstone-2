//NAVBAR
const logo = document.getElementById('homelogo');
const homeNavbar = document.getElementById('homeNavbar');
const prodNavbar = document.getElementById('prodNavbar');
const loginNavbar = document.getElementById('loginNavbar');


logo.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./adminDashboard.html')

})

homeNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./adminDashboard.html')

})
prodNavbar.addEventListener("click", (e) => {	e.preventDefault()

    window.location.replace('./userCatalog.html')

})

loginNavbar.addEventListener("click", (e) => {	e.preventDefault()

    localStorage.removeItem(`token`)
    localStorage.removeItem(`id`)
    localStorage.removeItem(`admin`)

    window.location.replace('./login.html')

})


// EDIT PRODUCT



const token = localStorage.getItem('token')
let params = new URLSearchParams(document.location.search);
const productId = params.get("productId")

const prodName = document.getElementById('name')
const desc = document.getElementById('description')
const weight = document.getElementById('weight')
const rate = document.getElementById('rate')
const prodLength = document.getElementById('length')
const qty = document.getElementById(`quantity`)

fetch(`https://dmb-steel.herokuapp.com/api/products/${productId}`, {
	method: "GET",
}).then(result => result.json())
.then(result => {

    prodName.value = result.name 
    desc.value = result.description
    weight.value = result.weight
    rate.value = result.rate 
    prodLength.value = result.length 
    qty.value = result.quantity
})



const editForm = document.getElementById(`editForm`);

editForm.addEventListener("submit", (e) => {
	e.preventDefault()


	fetch(`https://dmb-steel.herokuapp.com/api/products/${productId}/update`, {
		method: "PUT",
		headers:{
			"Content-Type": "application/json",
			"Authorization": `Bearer ${token}`
		},
		body: JSON.stringify({
			name: prodName.value,
			description: desc.value, 
            weight: weight.value,
            rate : rate.value,
            length : prodLength.value,
            quantity : qty.value
		})
	})
	.then(result => result.json())
	.then(result => {
		console.log(result)

		if(result){
			alert(`Product successfully updated`)

                window.location.replace('./userCatalog.html')
	
		} else {
			alert('Something went wrong')
		}
	})

})





