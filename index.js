const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config();

const cors = require('cors');

const PORT = process.env.PORT || 4013;
const app = express();

// CONNECT ROUTE MODULE

const userRoutes = require(`./routes/userRoutes`);
const productRoutes = require(`./routes/productRoutes`);
const orderRoutes = require(`./routes/orderRoutes`);
const cartRoutes = require(`./routes/cartRoutes`);


// MIDDLEWARES

app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(cors())

//MONGOOSE CONNECTION

mongoose.connect(process.env.MONGO_URL,{useNewUrlParser: true, useUnifiedTopology: true});

// DB CONNECTION NOTIFICATION

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to database`));


//ROUTES
const path = require('path');

app.use(`/api/users`, userRoutes);
app.use(`/api/products`, productRoutes);
app.use(`/api/orders`, orderRoutes);
app.use(`/api/carts`, cartRoutes);

app.use(express.static("./CLIENT/assets/pages"));
app.use(express.static(path.join(__dirname, './CLIENT/assets/js')));

app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname,  "./CLIENT/assets/pages/homepage.html"));
  });

app.listen(PORT, () => console.log(`Server connected to port ${PORT}`))
