const express = require('express');
const router = express.Router();
const {verify,verifyAdmin} = require('./../auth')

const {getProducts,
        activeProducts,
        list,
        product,
        updateProduct,
        archive,
        unarchive,
        deleteProduct} = require('./../controllers/productControllers')

//GET PRODUCT LIST

router.get(`/`, async(req, res) => {   
    try{
        await getProducts().then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    } 
})

//GET ACTIVE PRODUCTS

router.get('/isActive', async (req, res) => {
	try{
		await activeProducts().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


//CREATE PRODUCT LISTING

router.post(`/listProduct`, verifyAdmin, async(req, res) => {
    try{
        await list(req.body).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})


//FIND PRODUCT
router.get(`/:productId`, async(req, res) => {

    const productId = req.params.productId;

    try{
        await product(productId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    } 
})
    
//UPDATE PRODUCT

router.put(`/:productId/update`, verifyAdmin, async(req,res) => {

    const productId = req.params.productId;
    
    try{
        await updateProduct(productId, req.body).then(result => res.send(result))

    }catch(err){
        res.status(500).json(err)
    }
})

//ARCHIVE PRODUCT

router.patch(`/:productId/archive`, verify, async(req,res) => {

    const productId = req.params.productId;
    
    try{
        await archive(productId).then(result => res.send(result))

    }catch(err){
        res.status(500).json(err)
    }
})

//UNARCHIVE PRODUCT

router.patch(`/:productId/unarchive`, verifyAdmin, async(req,res) => {

    const productId = req.params.productId;
    
    try{
        await unarchive(productId).then(result => res.send(result))

    }catch(err){
        res.status(500).json(err)
    }
})

//DELETE PRODUCT

router.delete('/:productId/delete', verifyAdmin, async (req, res) => {

    const productId = req.params.productId;
    try{
        await deleteProduct(productId).then(result => res.send(result))

    }catch(err){
        res.status(500).json(err)
    }
})


//ROUTER
module.exports = router;