const express = require('express');
const router = express.Router();

const {verify,decode,verifyAdmin} = require('./../auth')

const {regUser, 
        checkEmail,
        getUsers, 
        profile,
        login,
        toAdmin,
        toUser,
        changePass,
        deleteUser} = require('./../controllers/userControllers')

//REGISTRATION

router.post('/register', async(req, res) => {

    try{
        await regUser(req.body).then(result => res.send(result))
    }catch(error){
        res.status(500).json(error)
    }
  
})

//CHECK IF EMAIL EXISTS
router.post(`/email-exists`, async (req, res) => {
    try{
        await checkEmail(req.body).then(result => res.send(result))
    }catch(error){
        res.status(500).json(error)
  }
})

//LOGIN

router.post('/login', async(req, res) => {
	try{
        await login(req.body).then(result => res.send(result))
    }catch(error){
        res.status(500).json(error)
  }
})



//GET ALL

router.get(`/allUsers`, verifyAdmin, async(req, res) => {   
    try{
        await getUsers().then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    } 
})


//USER PROFILE

router.get(`/profile`, verify, (req,res) =>{

    const userId = decode(req.headers.authorization).id

    try{
		profile(userId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

// USER TO ADMIN

router.patch('/toAdmin', verifyAdmin, async (req, res) => {
   
    try{
        await toAdmin(req.body).then(result => res.send(result))

    }catch(err){
        res.status(500).json(err)
    }
})

// ADMIN TO USER

router.patch('/toUser', verifyAdmin, async (req, res) => {
	try{
		await toUser(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}

})

// CHANGE PASSWORD

router.patch('/changepass', verify, async(req, res) => {
	const userId = decode(req.headers.authorization).id
	try{
		await changePass(userId, req.body).then(result=> res.send(result))
	}catch{
		res.status(500).json(err)
	}

})


//DELETE USER

router.delete(`/deleteUser`, verifyAdmin, async(req,res)=>{
    try{
        await deleteUser(req.body).then(result=>res.send(result))
    }catch(err){
		res.status(500).json(err)
	}
})

//ROUTER
module.exports = router;