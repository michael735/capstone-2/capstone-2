const { Router } = require('express');
const express = require('express');
const router = express.Router();
const {verify,decode,verifyAdmin} = require('../auth')

const {getList,
    createCart,
    myCart,
    cart,
    archiveCart,
    unarchiveCart,
    deleteCart} = require('../controllers/cartControllers');


//GET ALL ORDERS

router.get(`/allCarts`, verifyAdmin, async(req, res) => {   
    try{
        await getList().then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    } 
})

//CREATE CART

router.post(`/create`, verify, async(req,res) => {  

    const userId =  decode(req.headers.authorization).id

    try{
        await createCart(userId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }

})


//FIND USER'S CART LIST

router.get(`/myCart`, verify, async(req,res) => {


    const userId = decode(req.headers.authorization).id

    
    try{
        await myCart(userId).then(result => res.send(result))
        
    }catch(err){
        res.status(500).json(err)
    }
})

//GET AN SPECIFIC ORDER

router.get(`/:cartId`, verify, async(req,res)  => {

    const cartId = req.params.cartId;

    try{
        await cart(cartId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    } 

})

//ARCHIVE ORDER

router.patch(`/:cartId/archive`, verify, async(req,res) => {

    const cartId = req.params.cartId;
    
    try{
        await archiveCart(cartId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

//UNARCHIVE ORDER

router.patch(`/:cartId/unarchive`, verify, async(req,res) => {

    const cartId = req.params.cartId;
    
    try{
        await unarchiveCart(cartId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

router.delete('/:cartId/delete', verify, async (req, res) => {

    const cartId = req.params.cartId;
    try{
        await deleteCart(cartId).then(result => res.send(result))

    }catch(err){
        res.status(500).json(err)
    }
})

//ROUTER
module.exports = router;

