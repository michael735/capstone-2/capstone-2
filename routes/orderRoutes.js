const { Router } = require('express');
const express = require('express');
const router = express.Router();
const {verify,decode,verifyAdmin} = require('./../auth')

const {orderList,
        create,
        getList,
        archive,
        unarchive,
        deleteOrder,
        order,
        editQty} = require('./../controllers/orderControllers');


//GET ALL ORDERS

router.get(`/allList`, verifyAdmin, async(req, res) => {   
    try{
        await orderList().then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    } 
})

//CREATE ORDER

router.post(`/create`, verify, async(req,res) => {  

    const userId =  decode(req.headers.authorization).id

    try{
        await create(userId, req.body).then(result => res.send(result))
        
    }catch(err){
        res.status(500).json(err)
    }
})

//FIND USER'S ORDER LIST

router.get(`/myOrders`, verify, async(req,res) => {

    const userId = decode(req.headers.authorization).id
    
    try{
        await getList(userId).then(result => res.send(result))
        
    }catch(err){
        res.status(500).json(err)
    }
})

//GET AN SPECIFIC ORDER

router.get(`/:orderId`, verify, async(req,res)  => {

    const orderId = req.params.orderId;

    try{
        await order(orderId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    } 

})

//EDIT QUANTITY

router.patch(`/:orderId/editQty`, verify, async(req,res) => {

    const orderId = req.params.orderId;
  
    try{
        await editQty(orderId, req.body).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

//ARCHIVE ORDER

router.patch(`/:orderId/archive`, verify, async(req,res) => {

    const orderId = req.params.orderId;
    
    try{
        await archive(orderId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

//UNARCHIVE ORDER

router.patch(`/:orderId/unarchive`, verify, async(req,res) => {

    const orderId = req.params.orderId;
    
    try{
        await unarchive(orderId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

//DELETE ORDER

router.delete('/:orderId/delete', verify, async (req, res) => {

    const orderId = req.params.orderId;
    try{
        await deleteOrder(orderId).then(result => res.send(result))

    }catch(err){
        res.status(500).json(err)
    }
})

//ROUTER
module.exports = router;

