const req = require('express/lib/request');
const { restart } = require('nodemon');
const { findOneAndUpdate } = require('./../models/Order');
const Order = require('./../models/Order');
const Product = require('./../models/Product');

//GET ORDER LIST

module.exports.orderList = async() => {
    
    return await Order.find().then(result => result)
}

//CREATE ORDER
module.exports.create = async(userId, reqBody) => {

    const {productId,quantity} = reqBody

    const product = await Product.findById(productId)
    const price = product.price
    const description = product.description


    const newOrder = new Order ({   
        userId: userId,
        productId: reqBody.productId,
        description: description,
        price: price,
        quantity: reqBody.quantity,
        bill: (reqBody.quantity*price)    
    })

    const prodQuantity = product.quantity
    const newQuantity = (prodQuantity - newOrder.quantity)

    return await newOrder.save().then(result => {
    
		if(result){  
            if (newQuantity < 0){
                return `No stocks left`
            }else if (newQuantity === 0){
                Product.findByIdAndUpdate(productId, {$set:{quantity: newQuantity, IsActive: false}}).then((res,err) => res ? true : err) 
                return true
            }
            else{
                Product.findByIdAndUpdate(productId, {$set:{quantity: newQuantity }}).then((res,err) => res ? true : err) 
                return true
            }	
		} else { 
			if(result == null){
				return false
			}
		}
	})
}

//GET USER'S ORDER LIST

module.exports.getList = async(id) => {
   
    return await Order.find({"userId": id, "isActive" : false}).then(result => result)
}

//GET AN SPECIFIC ORDER
module.exports.order = async (id) => {

	return await Order.findById(id).then((result, err) => {
		if(result){
			return result
		}else{
			if(result == null){
				return {message: `Order does not exist`}
			} else {
				return err
			}
		}
	})
}

//EDIT ORDER QTY

module.exports.editQty = async (id, reqBody) => {

    
    const newQty = ({
                    quantity : reqBody.quantity,
                    price : reqBody.price,
                    bill : (reqBody.quantity*reqBody.price)
                    })

	return await Order.findByIdAndUpdate(id, {$set:newQty}, {new:true}).then((result, err) => {
		if(result){
            return result
		}
        else{
			if(result == null){
				return {message: `Order does not exist`}
			} else {
				return err
			}
		}
	})

}
//ARCHIVE ORDER

module.exports.archiveOrder= async(id) => {

	return await Order.findByIdAndUpdate(id, {$set:{isActive: false}}, {new:true}).then ((result,err) => result ? result : err)
}

//UNARCHIVE ORDER

module.exports.unarchive= async(id) => {

	return await Order.findByIdAndUpdate(id, {$set:{isActive: true}}, {new:true}).then ((result,err) => result ? result : err)
}

//DELETE ORDER

module.exports.deleteOrder = async(id) => {

	return await Order.findByIdAndDelete(id).then ((result,err) => result ? true : err)
}
