const { stringify } = require('nodemon/lib/utils');
const Cart = require('./../models/Cart');
const Order = require('./../models/Order');



//GET ORDER LIST

module.exports.getList  = async() => {
    
    return await Cart.find().then(result => result)
}


//CREATE CART

module.exports.createCart = async(id) => {
    const order = await Order.find({"userId": id , "isActive": false})

    const bills = order.map(result => result.bill)

    const totalBill = bills.reduce((acc, cur) => acc + cur)

    const newCart = new Cart ({
        userId: id,
        orders: order,
        totalBill: totalBill
    })

    return await newCart.save().then(result => {
		if(result){
			return true
		} else {
			if(result == null){
				return false
			}
		}
	})
}

//GET USER'S CART

module.exports.myCart= async(id) => {
   
    return await Cart.find({"userId": id}).then(result => result)
}


//GET AN SPECIFIC CART
module.exports.cart = async (id) => {

	return await Cart.findById(id).then((result, err) => {
		if(result){
			return result
		}else{
			if(result == null){
				return {message: `Cart does not exist`}
			} else {
				return err
			}
		}
	})
}
 
//ARCHIVE CART

module.exports.archiveCart= async(id) => {

	return await Cart.findByIdAndUpdate(id, {$set:{active: false}}, {new:true}).then ((result,err) => result ? result : err)
}

//UNARCHIVE CART

module.exports.unarchiveCart= async(id) => {

	return await Cart.findByIdAndUpdate(id, {$set:{active: true}}, {new:true}).then ((result,err) => result ? result : err)
}

//DELETE ORDER

module.exports.deleteCart = async(id) => {

	return await Cart.findByIdAndDelete(id).then ((result,err) => result ? true : err)
}
