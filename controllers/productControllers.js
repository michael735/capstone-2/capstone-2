const Product = require('./../models/Product');

//GET PRODUCT LIST

module.exports.getProducts = async () => {

	return await Product.find().then(result => result)
}

//GET ACTIVE PRODUCTS

module.exports.activeProducts = async () => {

	return await Product.find({isActive:true}).then(result => result)
}

//LIST PRODUCT

module.exports.list = async (reqBody) =>{

    const{name, description, weight, rate, length, quantity} = reqBody

    const newProduct = new Product({
        name: name,
        description: description,
        weight: weight,
        rate: rate,
        length: length,
        price: (length*rate*weight),
        quantity: quantity
    })

    return await newProduct.save().then(result =>{
        if(result){
            return true
        }else{
            if(result == null){
                return false
            }else {
                return result
            }
        }
    })
}

//GET PRODUCT
module.exports.product = async (id) => {

	return await Product.findById(id).then((result, err) => {
		if(result){
			return result
		}else{
			if(result == null){
				return {message: `Product does not exist`}
			} else {
				return err
			}
		}
	})
}

//UPDATE PRODUCT

module.exports.updateProduct = async (id, reqBody) => {

	const productDetails = ({
		name: reqBody.name,
		description: reqBody.description,
        length: reqBody.length,
		weight: reqBody.weight,
        rate: reqBody.rate,
        price: (reqBody.rate*reqBody.weight*reqBody.length),
        quantity: reqBody.quantity
	})


	return await Product.findByIdAndUpdate(id, {$set: productDetails}, {new:true}).then((result, err) => {
		if(result){
			return result
		}else{
			if(result == null){
				return {message: `Product does not exist`}
			} else {
				return err
			}
		}
	})

}

//ARCHIVE PRODUCT

module.exports.archive= async(id) => {

	return await Product.findByIdAndUpdate(id, {$set:{isActive: false}}, {new:true}).then ((result,err) => result ? result : err)
}

//UNARCHIVE PRODUCT

module.exports.unarchive= async(id) => {

	return await Product.findByIdAndUpdate(id, {$set:{isActive: true}}, {new:true}).then ((result,err) => result ? result : err)
}

//DELETE PRODUCT

module.exports.deleteProduct = async(id) => {

	return await Product.findByIdAndDelete(id).then ((result,err) => result ? true : err)
}


// module.exports.findActive = async () => {

// 	return await Product.find({"isActive": true}).then(result => result)
// }
