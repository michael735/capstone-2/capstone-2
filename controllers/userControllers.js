const User = require(`./../models/User`)
const AES = require("crypto-js/aes");
const CryptoJS = require("crypto-js");
const {createToken} = require('./../auth');

//REGISTER USERS

module.exports.regUser = async (reqBody) => {

    const {firstName,lastName,email, password} = reqBody

    const newUser = new User ({
		firstName: firstName,
		lastName: lastName,
        email: email,
		password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
    })

    
    return await newUser.save().then(result => {
		if(result){
			return true
		} else {
			if(result == null){
				return false
			}
		}
	})
}

//CHECK IF EMAIL ALREADY EXISTS

module.exports.checkEmail = async(reqBody) => {
    const {email} = reqBody
    return await User.findOne({email: email}).then((result, err) => {
        if(result){
            return true
        }else{
            if(result == null){
                return false
            }else {
                return err
            }
        }
    })
}

//GET USERS LISTS
module.exports.getUsers = async() => {
    
    return await User.find().then(result => result)
}


//LOGIN

module.exports.login = async (reqBody) => {

	return await User.findOne({email: reqBody.email}).then((result, err) =>{
		if (result === null){
			return {message: `Account does not exist`}

		}else{ 

			if (result !== null ){
				const password = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8);
				if(reqBody.password == password){
					return {token: createToken(result)}
				}else{
					return{auth: `Authentication Failed`}
				}
			}else{
				return err
			}
		}
	})
}

// GET USER PROFILE
module.exports.profile = async (id) => {

	return await User.findById(id).then((result, err) => {
		if(result){
			return result
		}else{
			if(result == null){
				return {message: `user does not exist`}
			} else {
				return err
			}
		}
	})
}

//USER TO ADMIN

module.exports.toAdmin = async(reqBody) => {

	const {email} = reqBody
	return await User.findOneAndUpdate({email: email}, {$set:{isAdmin: true}}, {new:true}).then ((result,err) => result ? result : err)
}

//ADMIN TO USER
module.exports.toUser = async(reqBody) => {

	const {email} = reqBody
	return await User.findOneAndUpdate({email: email}, {$set:{isAdmin: false}}, {new:true}).then ((result,err) => result ? result: err)
}

//CHANGE PASSWORD
module.exports.changePass = async(id, reqBody) => {

	const newPass = ({password: CryptoJS.AES.encrypt(reqBody.CryptoJSpassword, process.env.SECRET_PASS).toString()})

	return await User.findByIdAndUpdate(id, {$set: newPass}, {new:true}).then((result, err) => {
		if(result){
			result.password = "********"
			return result
		}else{
			if(result == null){
				return {message: `user does not exist`}
			} else {
				return err
			}
		}
	})
}

//DELETE USER

module.exports.deleteUser = async(reqBody) => {
	
	const {email} = reqBody
	return await User.findOneAndDelete({email: email}).then ((result,err) => result ? true: err)
}