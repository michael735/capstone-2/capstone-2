const mongoose = require(`mongoose`);

const orderSchema = new mongoose.Schema({

    userId:{
        type: mongoose.Schema.Types.ObjectId,
        ref: `User`,
    },
    productId:{
        type: mongoose.Schema.Types.ObjectId,
        ref: `Product`,
        required: [true, `Product to be ordered is required`]
    },
    description:{
        type: String
    },
    price:{
        type: Number,
        default: 1,
        min: 1
    },
    quantity:{
        type: Number,
        default: 1,
    },
    bill:{
        type: Number,
        default: 1,
    },
    purchasedOn:{
        type: Date,
        default: Date.now
    },
    isActive:{
        type: Boolean,
        default: false
    }
}, {timestamps: true})

module.exports = mongoose.model(`Order`, orderSchema);