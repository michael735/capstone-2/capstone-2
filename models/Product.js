const mongoose = require(`mongoose`);

const productSchema = new mongoose.Schema({
   
    name:{
        type: String,
        required: [true, `Product name is required`],   
        unique: true 
    },
    description:{
        type: String
    },
    weight:{
        type:Number
    },
    rate:{
        type: Number
    },
    price:{
        type: Number
    },
    length:{
        type: Number
    },
    quantity:{
        type: Number,
        default: 1
    },
    isActive:{
        type: Boolean,
        default: true
    },
    createdOn:{
        type:Date,
        default: Date.now
    }

}, {timestamps: true})

module.exports = mongoose.model(`Product`, productSchema);