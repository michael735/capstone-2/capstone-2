const mongoose = require(`mongoose`);

const userSchema = new mongoose.Schema({

    firstName:{
        type: String,
        required: [true, `First Name is required`]
    },
    lastName:{
        type: String,
        required: [true, `Last Name is required`]
    },
    email:{
        type: String,
        required: [true, `Email address is required`],
        unique: true
    },
    password: {
        type: String,
        required: [true, "Password is required"],
        minLength: 8,    
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    cart:[{
        cartId:{
            type: String,
        },
        billAmount:{
            type: Number,
            default: 1,
        },
        isActive:{
            type: Boolean,
            default: true
        }
    }]

}, {timestamps: true})

module.exports = mongoose.model(`User`, userSchema);